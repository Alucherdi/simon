﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreController : MonoBehaviour {

	public Text textScore;
	public int score;

	void Start () {
		score = 0;
		textScore.text = score.ToString();
	}

	void WonRound() {
		score ++;
		textScore.text = score.ToString();
	}

	void Loose() {
		score = 0;
	}
}
