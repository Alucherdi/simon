﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SimonController : MonoBehaviour {
	public GameObject[] buttons;
	public float delay;
	[HideInInspector]
	public List<int> indications;
	public int numberOfIndications;
	public int remainIndications;
	public GameObject ui;

	[HideInInspector]
	public int checkIterator;
	private int listIterator;

	private float initDelay;
	private Scene currentScene;
	Animator anim;

	void Start () {
		initDelay = 2f;
		listIterator = 0;
		checkIterator = 0;

		currentScene = SceneManager.GetActiveScene();
		anim = ui.GetComponent<Animator>();

		remainIndications = numberOfIndications;
		indications = new List<int>();
		//first indications
		for (int i = 0; i <numberOfIndications; i ++) {
			int newIndication = Mathf.RoundToInt(Random.Range(0, buttons.Length));
			indications.Add(newIndication);
		}

		Invoke("SC", 2);
	}
	
	void Update () {
		if (Input.GetKeyDown(KeyCode.A)) {
			NewRound();
		}
	}

	private void SC() {
		StartCoroutine("SimonSays");
	}

	public void NewRound() {
		numberOfIndications ++;
		remainIndications = numberOfIndications;
		listIterator = 0;
		int newIndication = Mathf.RoundToInt(Random.Range(0, buttons.Length));
		indications.Add(newIndication);
		StartCoroutine("SimonSays", 1);
	}

	private IEnumerator SimonSays() {
		while (remainIndications > 0) {
			int actualIndication = indications[listIterator];
			buttons[actualIndication].GetComponent<LightController>().TurnOn();
			remainIndications --;
			listIterator ++;
			yield return new WaitForSeconds(delay);
		}
	}

	// 0 = lost, 1 = keep playing, 2 = won round
	public int checkOrder(int button) {
		if (button == indications[checkIterator]) {
			if (checkIterator == indications.Count - 1) {
				SendMessage("WonRound");
				showUI("score");
				showUI("show");
				Invoke("StartNewRound", initDelay);
				return 2;
			}
			checkIterator ++;
			return 1;
		} else {
			showUI("showBad");
			Invoke("restartLevel",3.0f);
			SendMessage("Loose");
			return 0;
		}
	}

	void StartNewRound() {
		checkIterator = 0;
		NewRound();
	}

	void restartLevel(){
		SceneManager.LoadScene(currentScene.name);
	}

	void showUI(string name){
		anim.SetTrigger(name);
	}

}
