﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightController : MonoBehaviour {

	public int buttonNumber;
	public float turnOfSpeed;

	private Light buttonLight;
	private SimonController sc;

	void Start () {
		buttonLight = transform.GetComponentInChildren<Light>();
		sc = GameObject.Find("SimonController").GetComponent<SimonController>();
	}
	
	void Update () {
		if (buttonLight.intensity > 0) {
			buttonLight.intensity -= turnOfSpeed;
			if (buttonLight.intensity < 0) buttonLight.intensity = 0;
		}
	}

	void OnMouseDown() {
		TurnOn();
		switch (sc.checkOrder(buttonNumber)) {
			case 0: 
				Debug.Log("Perdiste");
				break;
			case 1:
				Debug.Log("Continua");
				break;
			case 2:
				Debug.Log("Ganaste ronda");
				break;
			default:
				break;
		}
	}

	public void TurnOn() {
		buttonLight.intensity = 15;
	}
}
